<?php

use newerton\fancybox\FancyBox;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    echo newerton\fancybox\FancyBox::widget([
        'target' => 'a[rel=fancybox]',
        'helpers' => false,
        'mouse' => false,
        'config' => [
            'maxWidth' => '90%',
            'maxHeight' => '90%',
            'playSpeed' => 7000,
            'padding' => 0,
            'fitToView' => false,
            'width' => '70%',
            'height' => '70%',
            'autoSize' => false,
            'closeClick' => false,
            'openEffect' => 'elastic',
            'closeEffect' => 'elastic',
            'closeBtn' => true,
            'openOpacity' => true,
            'arrows' =>  false,
            'helpers' => [
                'title' => ['type' => 'float'],
                'buttons' => ['arrows' =>  false],
                'thumbs' => ['width' => 68, 'height' => 50],
                'overlay' => [
                    'locked' => false,
                    'css' => [
                        'background' => 'rgba(0, 0, 0, 0.8)'
                    ]
                ]
            ],
        ]
    ]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            [
                'attribute' => 'preview',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a(Html::img($data->preview, ['width'=>'100','height'=>'100']), $data->preview, ['rel' => 'fancybox']);
                }
            ],
            /*[
                'attribute' => 'preview',
                'format' => ['image',['width'=>'150','height'=>'150']],
            ],*/

            'author.fio',
            [
                'attribute' => 'date',
                'format' => ['date', 'php:d M Y']
            ],
            'date_create',

            // 'date',
            // 'author_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' =>'Кнопки действий',
                'template' => '{update} {view} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('[ред]', $url, [
                            'title' => 'Редактировать',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('[удл]', $url, [
                            'title' =>'Удалить',
                            'data-method' => 'post',
                            'data-confirm'=>"Хотите удалить?",
                        ]);
                    },
                    'view' => function ($url, $model, $key) {
                        return  Html::a("[просм]",['view','id'=>$model->id],[
                            'title' =>'Просмотреть',
                            'data-toggle'=>"modal",
                            'data-target'=>"#bookDetail",
                            'data-title'=>"Детальная информация",
                        ]);
                    },
                ],
            ],



        ],
    ]); ?>

</div>

<?php
Modal::begin([
    'id' => 'bookDetail',
    'header' => '<h4 class="modal-title">sdfsdf</h4>',
]);

echo '...';

Modal::end();

$this->registerJs("
    $('#bookDetail').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title')
        var href = button.attr('href')
        modal.find('.modal-title').html(title)

        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data)
            });
        })
");
?>