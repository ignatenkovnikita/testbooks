<?php

use app\models\Author;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-sm-10">{input}</div><div class="col-sm-10">{error}</div>',
            'labelOptions' => ['class' => 'col-sm-1 control-label'],
        ],
    ]); ?>
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-3">
        <?= $form->field($model, 'author_id')->dropDownList(ArrayHelper::map(Author::find()->all(), 'id', 'fio'), ['prompt' => '--Выберите автора--'])->label(false)?>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
        <?= $form->field($model, 'name')->textInput(['placeholder' => 'название книги'])->label(false) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 control-label"><label>Дата выхода книги:</label></div>
        <div class="col-xs-2 col-sm-2 col-md-2">
            <?= $form->field($model, 'dateStart')->widget(DatePicker::classname(), ['dateFormat' => 'yyyy-MM-dd', 'language' => 'ru','options' => ['class' => 'form-control']])->label(false) ?>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
            <?= $form->field($model, 'dateEnd')->widget(DatePicker::classname(), ['dateFormat' => 'yyyy-MM-dd', 'language' => 'ru','options' => ['class' => 'form-control']])->label("по") ?>
        </div>
        <div class="col-xs-5 col-sm-5 col-md-5">
            <div class="form-group  pull-right">
                <?= Html::submitButton('искать', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
