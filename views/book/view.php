<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = $model->name;
?>
<div class="book-view">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'date_create',
            'date_update',
            [
                'attribute' => 'preview',
                'format' => ['image',['width'=>'150','height'=>'150']],
            ],
            'date',
            'author.fio',
        ],
    ]) ?>

</div>

